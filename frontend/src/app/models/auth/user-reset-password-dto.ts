export interface UserResetPasswordDTO {
    email: string;
}