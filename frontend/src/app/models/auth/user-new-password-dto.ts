export interface UserNewPasswordDTO {
    password: string;
    confirmPassword: string
}