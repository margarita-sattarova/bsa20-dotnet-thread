export interface DeletePost {
    authorId: number;
    body: string;
    previewImage: string;
}