import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User } from '../../models/user';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../../services/auth.service';
import { ImgurService } from '../../services/imgur.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';

@Component({
    selector: 'app-reset-page',
    templateUrl: './reset-page.component.html',
    styleUrls: ['./reset-page.component.sass']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    public user = {} as User;
    public loading = false;
    public imageFile: File;
    public password: string;
    public confirmPassword: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private location: Location,
        private userService: UserService,
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private imgurService: ImgurService
    ) { }

    public ngOnInit() {
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public saveNewInfo() {
        const userSubscription = !this.imageFile
            ? this.userService.updateUser(this.user)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.user.avatar = imageData.body.data.link;
                    return this.userService.updateUser(this.user);
                })
            );

        this.loading = true;

        userSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.authService.setUser(this.user);
                this.snackBarService.showUsualMessage('Successfully updated');
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public goBack = () => window.location.replace('http://localhost:4200/');

    async delay(ms: number) {
        await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
    }

    public resetPass() {
        this.authService
            .resetPassword({ password: this.password, confirmPassword: this.confirmPassword })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => {
                this.snackBarService.showUsualMessage("Your password successfuly changed");
                this.delay(90000);
                this.goBack();
            },
                (error) => this.snackBarService.showErrorMessage(error));
    }
}