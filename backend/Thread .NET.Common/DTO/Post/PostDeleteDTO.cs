﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Thread_.NET.Common.DTO.Post
{
        public sealed class PostDeleteDTO
        {
            [JsonIgnore]
            public int AuthorId { get; set; }

            public string PreviewImage { get; set; }
            public string Body { get; set; }
        }
}
