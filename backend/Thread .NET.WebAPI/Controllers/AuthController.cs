﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Web;
using Thread_.NET.BLL.JWT;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Security;
using Thread_.NET.Interfaces;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _authService;
        private readonly UserService _userService;
        private readonly JwtFactory _jwtFactory;
        private readonly IMailer _mailer;

        public AuthController(AuthService authService, IMailer mailer, JwtFactory jwtFactory, UserService userService)
        {
            _authService = authService;
            _mailer = mailer;
            _jwtFactory = jwtFactory;
            _userService = userService;
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthUserDTO>> Login(UserLoginDTO dto)
        {
            return Ok(await _authService.Authorize(dto));
        }

        [HttpPost("sendLink")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(UserResetPasswordDTO dto)
        {
            var user = await _authService.FindUserByEmail(dto);
            var code = await _authService.GenerateAccessToken(Convert.ToInt32(user.Id), user.UserName, user.Email);
            var url = $"http://localhost:4200/resetPass?code={code.AccessToken.Token}";

            await _mailer.SendEmailAsync(user.Email, "Reset Password Link", $"Для сброса пароля пройдите по ссылке: <a href='{url}'>link</a>");

            return Ok();
        }

        [HttpPost("resetPass")]
        public async Task<IActionResult> ResetPassword(ChangePasswordRequest request)
        {
            Uri myUri = new Uri(request.Link);
            string token = HttpUtility.ParseQueryString(myUri.Query).Get(0);

            var userId = _jwtFactory.GetUserIdFromToken(token, "DD70E219DCF6408A7506EA0186D183AE");
            var userDTO = await _userService.GetUserById(userId);
            var userEntity = await _userService.GetUserEntityByID(userId);

            if (request.DTO.Password != request.DTO.ConfirmPassword)
                throw new InvalidOperationException($"'Password' and 'Confirm Password' do not match!");

            if (string.IsNullOrWhiteSpace(request.DTO.Password))
                throw new InvalidOperationException("Invalid password (password cannot be an empty string)!");

            var salt = SecurityHelper.GetRandomBytes();
            userEntity.Salt = Convert.ToBase64String(salt);
            userEntity.Password = SecurityHelper.HashPassword(request.DTO.Password, salt);

            await _userService.UpdateUser(userDTO);

            return Ok();
        }
    }

    public class ChangePasswordRequest
    {
        public string Link { get; set; }
        public UserNewPasswordDTO DTO { get; set; }
    }
}